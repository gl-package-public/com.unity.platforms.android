using System.IO;

namespace Unity.Build.Android.Classic
{
    sealed class AndroidArtifact : IBuildArtifact
    {
        public FileInfo OutputTargetFile;
        public DirectoryInfo OutpuDirectoryInfo;
    }
}
