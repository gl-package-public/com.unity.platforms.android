﻿using System.Collections.Generic;
using Unity.Build.Classic.Private;

namespace Unity.Build.Android.Classic
{
    internal class AndroidAdditionalFile
    {
        internal string SourceFile { get; }
        internal string DestinationFile { get; }

        internal AndroidAdditionalFile(string sourceFile, string destinationFile)
        {
            SourceFile = sourceFile;
            DestinationFile = destinationFile;
        }
    }

    internal class AndroidAdditionalFiles
    {
        internal IReadOnlyList<AndroidAdditionalFile> Files { get; }

        internal AndroidAdditionalFiles(IReadOnlyList<AndroidAdditionalFile> files)
        {
            Files = files;
        }
    }

    sealed class AndroidCacheAdditionallyProvidedFilesStep : BuildStepBase
    {
        public override BuildResult Run(BuildContext context)
        {
            var classicSharedData = context.GetValue<ClassicSharedData>();

            // Adjust streaming assets folder
            classicSharedData.StreamingAssetsDirectory = "src/main/assets";
            context.SetValue(classicSharedData);

            foreach (var customizer in classicSharedData.Customizers)
                customizer.OnBeforeRegisterAdditionalFilesToDeploy();

            var files = new List<AndroidAdditionalFile>();
            foreach (var customizer in classicSharedData.Customizers)
            {
                customizer.RegisterAdditionalFilesToDeploy((from, to) =>
                {
                    files.Add(new AndroidAdditionalFile(from, to));
                });
            }

            context.SetValue(new AndroidAdditionalFiles(files));
            return context.Success();
        }
    }
}
