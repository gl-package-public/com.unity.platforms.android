using System;
using Unity.Build.Common;
using Unity.Build.Classic;
using UnityEditor;

namespace Unity.Build.Android.Classic
{
    sealed class AndroidApplySettingsStep : BuildStepBase
    {
        public override Type[] UsedComponents { get; } =
        {
            //typeof(GeneralSettings),
            typeof(AndroidExportSettings),
            typeof(ClassicBuildProfile),
            typeof(AndroidArchitectures),
            typeof(AndroidAPILevels),
            typeof(ClassicScriptingSettings),
            typeof(ApplicationIdentifier),
            typeof(AndroidBundleVersionCode),
            typeof(AndroidKeystore)
        };

        public override BuildResult Run(BuildContext context)
        {
            var architectures = context.GetComponentOrDefault<AndroidArchitectures>().Architectures;
            var apiLevels = context.GetComponentOrDefault<AndroidAPILevels>();
            var profile = context.GetComponentOrDefault<ClassicBuildProfile>();
            var exportSettings = context.GetComponentOrDefault<AndroidExportSettings>();
            switch (profile.Configuration)
            {
                case BuildType.Debug:
                    EditorUserBuildSettings.androidBuildType = AndroidBuildType.Debug;
                    break;
                case BuildType.Develop:
                    EditorUserBuildSettings.androidBuildType = AndroidBuildType.Development;
                    break;
                case BuildType.Release:
                    EditorUserBuildSettings.androidBuildType = AndroidBuildType.Release;
                    break;
            }
            EditorUserBuildSettings.exportAsGoogleAndroidProject = exportSettings.ExportProject;

            // App bundle
            EditorUserBuildSettings.buildAppBundle = exportSettings.TargetType == AndroidTargetType.AndroidAppBundle;
            PlayerSettings.Android.useAPKExpansionFiles = exportSettings.TargetType == AndroidTargetType.AndroidAppBundle;
            PlayerSettings.Android.minifyRelease = exportSettings.TargetType == AndroidTargetType.AndroidAppBundle;

            // Unset ARM64 if we're targeting Mono
            var scriptingSettings = context.GetComponentOrDefault<ClassicScriptingSettings>();
            if (scriptingSettings.ScriptingBackend == ScriptingImplementation.Mono2x)
                architectures &= ~AndroidArchitecture.ARM64;

            PlayerSettings.Android.targetArchitectures = (UnityEditor.AndroidArchitecture)architectures;
            PlayerSettings.Android.minSdkVersion = (UnityEditor.AndroidSdkVersions)apiLevels.MinAPILevel;
            PlayerSettings.Android.targetSdkVersion = (UnityEditor.AndroidSdkVersions)apiLevels.TargetAPILevel;

            var applicationIdentifier = context.GetComponentOrDefault<ApplicationIdentifier>();
            PlayerSettings.SetApplicationIdentifier(BuildTargetGroup.Android, applicationIdentifier.PackageName);

            var bundleVersionCode = context.GetComponentOrDefault<AndroidBundleVersionCode>();
            PlayerSettings.Android.bundleVersionCode = bundleVersionCode.VersionCode;

            var keystore = context.GetComponentOrDefault<AndroidKeystore>();
            PlayerSettings.Android.useCustomKeystore = keystore.KeyaliasPass != String.Empty;
            PlayerSettings.Android.keystoreName = keystore.KeystoreFullPath;
            PlayerSettings.Android.keystorePass = keystore.KeystorePass;
            PlayerSettings.Android.keyaliasName = keystore.KeyaliasName;
            PlayerSettings.Android.keyaliasPass = keystore.KeyaliasPass;

            //var generalSettings = context.GetComponentOrDefault<GeneralSettings>();
            //PlayerSettings.bundleVersion = generalSettings.Version.ToString();

#if UNITY_2021_1_OR_NEWER
            EditorUserBuildSettings.androidCreateSymbols = (UnityEditor.AndroidCreateSymbols) exportSettings.CreateSymbols;
#else
            EditorUserBuildSettings.androidCreateSymbolsZip = exportSettings.CreateSymbols != AndroidCreateSymbols.Disabled;
#endif
            return context.Success();
        }
    }
}
