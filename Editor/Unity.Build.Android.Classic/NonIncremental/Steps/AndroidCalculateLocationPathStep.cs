using System.IO;
using Unity.Build.Classic;
using Unity.Build.Classic.Private;
using Unity.Build.Common;

namespace Unity.Build.Android.Classic
{
    class AndroidCalculateLocationPathStep : CalculateLocationPathStep
    {
        protected override string CalculatePath(BuildContext context)
        {
            var gradleOutput = context.GetValue<AndroidNonIncrementalData>().GradleOuputDirectory;
            // Set AndroidProjectArtifact, since there's no better place
            var gradleArtifact = context.GetOrCreateBuildArtifact<AndroidProjectArtifact>();
            gradleArtifact.ProjectDirectory = new DirectoryInfo(gradleOutput);

            var outputPath = context.GetOutputBuildDirectory();
            var exportSettings = context.GetComponentOrDefault<AndroidExportSettings>();
            if (exportSettings.ExportProject)
            {
                return outputPath;
            }
            else
            {
                var productName = context.GetComponentOrDefault<GeneralSettings>().ProductName;
                var outputFilePath = Path.Combine(outputPath, productName);
                switch (exportSettings.TargetType)
                {
                    case AndroidTargetType.AndroidAppBundle:
                        outputFilePath += ".aab";
                        break;
                    case AndroidTargetType.AndroidPackage:
                        outputFilePath += ".apk";
                        break;
                }

                return outputFilePath;
            }
        }
    }
}
