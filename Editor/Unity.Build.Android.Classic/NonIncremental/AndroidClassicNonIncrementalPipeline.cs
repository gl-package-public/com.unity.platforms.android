using System;
using System.IO;
using Unity.Build;
using Unity.Build.Classic;
using Unity.Build.Classic.Private;
using Unity.Build.Common;
using UnityEditor;

#if UNITY_ANDROID
using UnityEditor.Android;
#endif

namespace Unity.Build.Android.Classic
{
    sealed class AndroidClassicNonIncrementalPipeline : ClassicNonIncrementalPipelineBase
    {
#if UNITY_ANDROID
        // Note: Using IPostGenerateGradleAndroidProject to inject additional files into Android build
        //       Instead of manually compiling gradle project, since otherwise it would require us to reimplement too many things (gradle invocation, symbol packing, etc)
        class PostGenerateProcessor : IPostGenerateGradleAndroidProject
        {
            internal static BuildContext Context { set; get; }
            public int callbackOrder => 0;

            public void OnPostGenerateGradleAndroidProject(string path)
            {
                if (Context == null)
                    return;

                var files = Context.GetValue<AndroidAdditionalFiles>().Files;
                foreach (var file in files)
                {
                    var destination = Path.Combine(path, file.DestinationFile);
                    var parent = Path.GetDirectoryName(destination);
                    Directory.CreateDirectory(parent);
                    File.Copy(file.SourceFile, destination, true);
            }
        }
        }
#endif

        public override Platform Platform => Platform.Android;

        public override BuildStepCollection BuildSteps { get; } = new[]
        {
            typeof(SaveScenesAndAssetsStep),
            typeof(ApplyUnitySettingsStep),
            typeof(AndroidApplySettingsStep), // Note: Must go after ApplyUnitySettingsStep !!
            typeof(AndroidCalculateLocationPathStep),
            // Note: Cannot use CopyAdditionallyProvidedFilesStep after BuildPlayerStep, since apk is already produced and no extra files can be added.
            //       Also cannot use CopyAdditionallyProvidedFilesStep from IPostGenerateGradleAndroidProject, since apparently dots uses UnityEditor.Build.Pipeline.ContentPipeline:BuildAssetBundles
            //       that cause Cannot call ... while a build in progress
            //       What we do is we cache whatever files we'll need to copy from AndroidCacheAdditionallyProvidedFilesStep and then perform the actualcopy via AndroidCopyAdditionallyProvidedFilesStep
            typeof(AndroidCacheAdditionallyProvidedFilesStep),
            typeof(BuildPlayerStep),
            typeof(AndroidProduceArtifactStep)
        };

        protected override void PrepareContext(BuildContext context)
        {
            base.PrepareContext(context);
            var androidNonIncrementalData = new AndroidNonIncrementalData(context);
            context.SetValue(androidNonIncrementalData);

            var classicData = context.GetValue<ClassicSharedData>();
            classicData.StreamingAssetsDirectory = Path.Combine(androidNonIncrementalData.GradleOuputDirectory, "unityLibrary/src/main/assets");

            // TODO: External\Android\NonRedistributable\ndk\builds\platforms doesn't contain android-16, which is used as default in Burst
            if (Unsupported.IsSourceBuild())
            {
#if UNITY_2019_3_OR_NEWER
                Environment.SetEnvironmentVariable("BURST_ANDROID_MIN_API_LEVEL", $"{21}");
#endif
            }
        }

        protected override BuildResult OnBuild(BuildContext context)
        {
#if UNITY_ANDROID
            try
            {
                PostGenerateProcessor.Context = context;
                return base.OnBuild(context);
            }
            finally
            {
                PostGenerateProcessor.Context = null;
            }
#else
            return base.OnBuild(context);
#endif
        }

        protected override BoolResult OnCanRun(RunContext context)
        {
#if UNITY_ANDROID
            var artifact = context.GetBuildArtifact<AndroidArtifact>();
            if (artifact == null)
            {
                return BoolResult.False($"Could not retrieve build artifact '{nameof(AndroidArtifact)}'.");
            }

            if (artifact.OutputTargetFile == null && artifact.OutpuDirectoryInfo == null)
            {
                return BoolResult.False($"Both {nameof(AndroidArtifact.OutputTargetFile)} and {nameof(AndroidArtifact.OutpuDirectoryInfo)} are null.");
            }

            if (artifact.OutpuDirectoryInfo != null)
            { 
                return BoolResult.False($"Running exported project '{artifact.OutpuDirectoryInfo.FullName}' is not supported");
            }

            if (artifact.OutputTargetFile != null)
            {
                var fileName = artifact.OutputTargetFile.FullName;
                var extension = Path.GetExtension(fileName);
                if (extension == ".aab")
                    return BoolResult.False($"Running .aab '{fileName}' is not supported");
                if (extension != ".apk")
                    return BoolResult.False($"Expected .apk in path, but got '{fileName}'.");
            }

            return BoolResult.True();
#else
            return BoolResult.False("Active Editor platform has to be set to Android.");
#endif
        }

        protected override RunResult OnRun(RunContext context)
        {
            AndroidClassicPipelineShared.SetupPlayerConnection(context);

#if UNITY_ANDROID
            var artifact = context.GetBuildArtifact<AndroidArtifact>();
            var fileName = artifact.OutputTargetFile.FullName;
            var path = $"\"{Path.GetFullPath(fileName)}\"";
            var adb = ADB.GetInstance();
            try
            {
                EditorUtility.DisplayProgressBar("Installing", $"Installing {path}", 0.3f);
                adb.Run(new[] { "install", "-r", "-d", path }, $"Failed to install '{fileName}'");
            }
            catch (Exception ex)
            {
                return context.Failure(ex);
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
            UnityEngine.Debug.Log($"{path} successfully installed.");

            var applicationIdentifier = context.GetComponentOrDefault<ApplicationIdentifier>();
            var runTarget = $"\"{applicationIdentifier.PackageName}/com.unity3d.player.UnityPlayerActivity\"";
            try
            {
                EditorUtility.DisplayProgressBar("Launching", $"Launching {runTarget}", 0.6f);
                adb.Run(new[]
                {
                    "shell", "am", "start",
                    "-a", "android.intent.action.MAIN",
                    "-c", "android.intent.category.LAUNCHER",
                    "-f", "0x10200000",
                    "-S",
                    "-n", runTarget
                }, $"Failed to launch {runTarget}");
            }
            catch (Exception ex)
            {
                return context.Failure(ex);
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
            UnityEngine.Debug.Log($"{runTarget} successfully launched.");

            return context.Success(new AndroidRunInstance());
#else
            return context.Failure("Active Editor platform has to be set to Android.");
#endif
        }

        public override DirectoryInfo GetOutputBuildDirectory(BuildConfiguration config)
        {
            if (config.HasComponent<InstallInBuildFolder>())
            {
                var path = UnityEditor.BuildPipeline.GetPlaybackEngineDirectory(BuildTarget.Android, BuildOptions.None);
                path = Path.Combine(path, "SourceBuild", config.GetComponentOrDefault<GeneralSettings>().ProductName);
                return new DirectoryInfo(path);
            }
            else
            {
                return base.GetOutputBuildDirectory(config);
            }
        }
    }
}
