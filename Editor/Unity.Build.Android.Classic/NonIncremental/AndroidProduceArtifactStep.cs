using System.IO;
using BuildReport = UnityEditor.Build.Reporting.BuildReport;

namespace Unity.Build.Android.Classic
{
    [BuildStep(Description = "Producing Android Artifacts")]
    sealed class AndroidProduceArtifactStep : BuildStepBase
    {
        public override BuildResult Run(BuildContext context)
        {
            var report = context.GetValue<BuildReport>();
            if (report == null)
            {
                return context.Failure($"Could not retrieve {nameof(BuildReport)} from build context.");
            }

            var exportSettings = context.GetComponentOrDefault<AndroidExportSettings>();
            var artifact = context.GetOrCreateBuildArtifact<AndroidArtifact>();
            if (exportSettings.ExportProject)
            {
                artifact.OutpuDirectoryInfo = new DirectoryInfo(report.summary.outputPath);
                artifact.OutputTargetFile = null;
            }
            else
            {
                artifact.OutpuDirectoryInfo = null;
                artifact.OutputTargetFile = new FileInfo(report.summary.outputPath);
            }

            return context.Success();
        }
    }
}
